module github.com/Excel-MEC/excelplay-backend-dalalbull

go 1.13

require (
	github.com/Excel-MEC/excelplay-backend-dalalbull v0.0.0-20200318041724-44b2d42efcad
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.5.2
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.6.0
)
